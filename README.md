
Design-Space Exploration of a VLIW Processor
============================================

ASCI Spring School 2017
-----------------------

This repository contains the workload for the DSE assignment and some build
scripts to get you started.


Installation
------------

1.  You need a Linux environment for the tools, with *32-bit* GCC build
    tools and Make installed. You also need Python 2.7+.

2.  Download the contents of this repository if you're reading this on
    Bitbucket:

    `git clone https://bitbucket.org/jvanstraten/asci-vliw-dse.git`

3.  Download the VEX 3.43 release from HP here:
    http://www.hpl.hp.com/downloads/vex/

4.  Untar the VEX release in the root of where you saved the contents of the
    Bitbucket repository, or the build scripts won't work.

5.  Unfortunately, VEX does not work out of the box on many systems because it
    has paths to /lib32 baked into it. If your system does not provide this
    directory, you can symlink it to /lib as follows:

    `sudo ln -s lib /lib32`

6.  You should now be able to run make to compile and simulate the workload
    with the default processor configuration.


Symptoms of problems
--------------------

(just in case)

### Missing /lib32 symlink

    ls: cannot access /lib32/libc.so.?: No such file or directory

You need to make the /lib32 symlink (step 5 of the installation instructions).

### Missing 32-bit gcc

    .../ld: skipping incompatible .../libdl.so when searching for -ldl
    .../ld: cannot find -ldl
    .../ld: skipping incompatible .../libgcc.a when searching for -lgcc
    .../ld: cannot find -lgcc
    (and so on)

You have 64-bit GCC installed, you need to get the 32-bit version. Note that you
do not have to uninstall the 64-bit version or anything, they will work together
just fine.

Documentation
-------------

The assignment description can be found in `assignment.pdf`. The VEX release
also contains documentation, which can be found in `vex-3.42/share/doc/vex.pdf`.
You will also want to read through the `Makefile`, because you will most likely
want to adapt it when you start automating the DSE process. Finally, feel free
to read through the `config.py` script that generates the VEX configuration
files, but please do not modify the area/energy computation algorithm.


Getting help
------------

If you have questions, e-mail either or both:

    "Jeroen van Straten" <j.vanstraten-1@tudelft.nl>
    "Joost Hoozemans" <j.j.hoozemans@tudelft.nl>

