#!/usr/bin/env python

from __future__ import print_function
import sys

if len(sys.argv) < 3:
	print('Usage: %s <filename> <key>' % sys.argv[0], file=sys.stderr)
	print('')
	print('Prints the value for the given key in a space-separated key-value file.')
	print('Returns 1 if the file does not exist or the key is not found in the file.')
	sys.exit(2)

with open(sys.argv[1], 'r') as f:
	data = f.read().split('\n')

for line in data:
	line = line.split(None, 1)
	if len(line) < 2:
		continue
	key, value = line
	if key.strip() == sys.argv[2]:
		print(value.strip())
		sys.exit(0)

sys.exit(1)

