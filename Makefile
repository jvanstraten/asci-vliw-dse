
# Tip: run me using -j to parallelize compilation

# --- Configuration ---

# VEX toolchain directory
VEX = vex-3.43

# File with a list of programs that constitute our workload followed by
# compiler flags
WORKLOAD = workload

# Processor configuration file
CONFIG = config.ini

# Source directory
SRCDIR = powerstone

# Result output directory
OUTDIR = output


# --- Internal variables ---

# Prevent make from removing intermediate files
.SECONDARY:

# Disable implicit rules
.SUFFIXES:

# Read the list of programs that constitute our workload
PROGRAMS = $(shell cut -f1 -d" " $(WORKLOAD))

# Compiler command line
CC = $(realpath $(VEX)/bin/cc)

# Configuration script
CONFIG_PY = config.py

# Products of the config.py script
# NOTE: config.py generates all of the files listed here (also the commented
# ones), but if you specify all of them Make will stubbornly execute the script
# several times. This is a problem when using -j because config.py is not
# entirely thread-safe.
CONFIG_OUT = $(OUTDIR)/vex.bin
#CONFIG_OUT += $(OUTDIR)/vex.mm
#CONFIG_OUT += $(OUTDIR)/vex.cfg
#CONFIG_OUT += $(OUTDIR)/cluster.count
#CONFIG_OUT += $(OUTDIR)/config.ini

# Area-energy-delay model tool
AED = ./aed

# Required compiler flags
CFLAGS = -fmm=../vex.mm
CFLAGS += -width `cat ../cluster.count`
CFLAGS += -ms
CFLAGS += -fno-xnop -fexpand-div -c99inline


# --- User command rules ---

# Run all benchmarks (default recipe)
.PHONY: all
all: $(OUTDIR)/vex.bin $(patsubst %,$(OUTDIR)/performance-%.rpt,$(PROGRAMS))
	$(AED) $^ > $(OUTDIR)/aed.rpt
	@cat $(OUTDIR)/aed.rpt

# Run only the specified benchmark
.PHONY: $(PROGRAMS)
$(PROGRAMS): %: $(OUTDIR)/vex.bin $(OUTDIR)/performance-%.rpt
	$(AED) $^ > $(OUTDIR)/aed-$*.rpt
	@cat $(OUTDIR)/aed-$*.rpt

# Removes output files
.PHONY: clean
clean:
	rm -rf $(OUTDIR)/*/
	rm -f $(OUTDIR)/*.rpt
	rm -f $(CONFIG_OUT)
	rm -f $(OUTDIR)/cluster.count
	rm -f $(OUTDIR)/config.ini
	rm -f $(OUTDIR)/vex.cfg
	rm -f $(OUTDIR)/vex.mm


# --- Intermediate rules ---

# Runs the configuration script
$(CONFIG_OUT): $(CONFIG_PY) $(CONFIG)
	python $^ $(OUTDIR)

# Compile the simulator for the given benchmark
$(OUTDIR)/%/csim: $(SRCDIR)/%.c $(CONFIG_OUT) $(WORKLOAD)
	mkdir -p $(OUTDIR)/$*
	ln -sf ../vex.cfg $(OUTDIR)/$*/vex.cfg
	rm -f $(OUTDIR)/$*/ta.log.*
	cd $(OUTDIR)/$* && $(CC) $(CFLAGS) \
		`$(realpath lookup.py) $(realpath $(WORKLOAD)) $*` \
		$(realpath $<) -o $(notdir $@)

# Run the compiled simulator for the given benchmark
$(OUTDIR)/%/stdout $(OUTDIR)/performance-%.rpt: $(OUTDIR)/%/csim $(CONFIG_OUT)
	cd $(OUTDIR)/$* && $(realpath $<) > $(abspath $(OUTDIR)/$*/stdout)
	cp $(OUTDIR)/$*/ta.log.000 $(OUTDIR)/performance-$*.rpt

