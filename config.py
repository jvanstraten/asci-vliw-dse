#!/usr/bin/env python

from __future__ import print_function
import sys, os, math, struct


# --- Define config file wrapper classes and helper functions ---
def warn(line_nr, fmt, *args):
	"""Prints a warning message to stderr."""
	print('Warning: %s:%d: %s' % (cfg_fname, line_nr, fmt % args), file=sys.stderr)

def error(line_nr, fmt, *args):
	"""Prints an error message to stderr and exits."""
	print('Error: %s:%d: %s' % (cfg_fname, line_nr, fmt % args), file=sys.stderr)
	sys.exit(1)


def chk_range(min=0, max=None):
	def chk(value):
		if value < min:
			return 'must be at least %d' % min
		if max is not None and value > max:
			return 'must be at most %d' % max
		return None
	return chk

def chk_pow2(min=1, max=None):
	def chk(value):
		if value < min:
			return 'must be at least %d' % min
		if max is not None and value > max:
			return 'must be at most %d' % max
		if (value & (value - 1)) != 0:
			return 'must be a power of two' % value
		return None
	return chk

def log2(i):
	return i.bit_length() - 1


class Schema:
	"""Represents the expected contents of a config.ini section."""

	def __init__(self):
		self.data = {}

	def append(self, key, chk_fn=None):
		if chk_fn is None:
			chk_fn = lambda x: None
		self.data[key] = chk_fn

	def __getitem__(self, key):
		return self.data.get(key, None)

	def __iter__(self):
		return iter(self.data)


class Section:
	"""Represents a config.ini section."""

	def __init__(self, schema, line_nr):
		self.schema = schema
		self.data = {}
		self.line_nr = line_nr

	def append(self, key, value, line_nr):
		"""Appends data to this section."""

		# Check for redefinitions
		cur = self.data.get(key, None)
		if cur is not None:
			_, cur_line = cur
			warn(line_nr,
				'"%s" redefined, previously defined on line %d',
				key, cur_line)

		# Parse the value
		try:
			if value.endswith('k'):
				value = int(value[:-1].strip()) * 1024
			else:
				value = int(value.strip())
		except ValueError:
			warn(line_nr,
				'failed to parse value for "%s" as int, ignoring line',
				key)
			return

		# Check the value against the schema
		chk_fn = self.schema[key]
		if chk_fn is None:
			warn(line_nr,
				'unknown key "%s", ignoring line',
				key)
			return
		msg = chk_fn(value)
		if msg is not None:
			warn(line_nr,
				'key "%s": %s, ignoring line',
				key, msg)
			return

		# Store the value
		self.data[key] = (value, line_nr)

	def check(self):
		"""Checks whether all keys in the schema have been loaded."""
		ok = True
		for key in self.schema:
			if key not in self.data:
				warn(self.line_nr,
					'required key "%s" is missing in this section',
					key)
				ok = False
		if not ok:
			error(self.line_nr,
				'required keys are missing in this section')

	def __getattr__(self, key):
		"""Returns the value for the given key."""
		data = self.data.get(key)
		if data is None:
			error(self.line_nr,
				'missing data for key "%s" in this section',
				key)
		value, line_nr = data
		return value


class Transposer:
	"""Wraps a list of Section instances as one, such that attribute queries
	return a list with one item for each instance."""

	def __init__(self, data):
		self.data = data

	def __getattr__(self, key):
		return [getattr(d, key) for d in self.data]

	def __len__(self):
		return len(self.data)


class ConfigFile:
	"""Represents a config.ini file."""

	def __init__(self):
		self.icache = None
		self.dcache = None
		self.fetch = None
		self.cluster = []
		self.current = None

		self.clusters = Transposer(self.cluster)

		s = Schema()
		s.append('size', chk_pow2(128))
		s.append('sets', chk_pow2(1, 4))
		s.append('line', chk_pow2(8))
		self.icache_schema = s

		s = Schema()
		s.append('size', chk_pow2(128))
		s.append('sets', chk_pow2(1, 4))
		s.append('line', chk_pow2(4))
		s.append('load', chk_range(1))
		s.append('store', chk_range(1))
		self.dcache_schema = s

		s = Schema()
		s.append('issue', chk_range(2))
		self.fetch_schema = s

		s = Schema()
		s.append('issue', chk_range(2))
		s.append('alu', chk_range(1))
		s.append('mul', chk_range(1))
		s.append('mem', chk_range(1))
		s.append('send', chk_range(1))
		s.append('receive', chk_range(1))
		s.append('gp', chk_range(16, 64))
		s.append('br', chk_range(1, 8))
		self.cluster_schema = s

	def append_data(self, key, value, line_nr):
		"""Appends data to the current section."""

		if self.current is None:
			warn(line_nr,
				'ignoring key-value line before section header')
			return

		if self.current is False:
			return

		self.current.append(key, value, line_nr)

	def append_section(self, name, line_nr):
		"""Appends a section."""

		def chk_redef(section):
			if section is not None:
				warn(line_nr,
					'[%s] redefined, previously defined on line %d',
					name, section.line_nr)

		if name == 'icache':
			chk_redef(self.icache)
			self.icache = Section(self.icache_schema, line_nr)
			self.current = self.icache

		elif name == 'dcache':
			chk_redef(self.dcache)
			self.dcache = Section(self.dcache_schema, line_nr)
			self.current = self.dcache

		elif name == 'fetch':
			chk_redef(self.fetch)
			self.fetch = Section(self.fetch_schema, line_nr)
			self.current = self.fetch

		elif name == 'cluster':
			self.cluster.append(Section(self.cluster_schema, line_nr))
			self.current = self.cluster[-1]

		else:
			warn(line_nr,
				'ignoring unknown section [%s]',
				name)
			self.current = False

	def check(self):
		"""Checks whether all keys have been loaded with proper values."""

		if self.icache is None:
			error(0, '[icache] section is missing')
		self.icache.check()

		if self.dcache is None:
			error(0, '[dcache] section is missing')
		self.dcache.check()

		if self.fetch is None:
			error(0, '[fetch] section is missing')
		self.fetch.check()

		if len(self.cluster) == 0:
			error(0, 'there must be at least one [cluster] section')
		for section in self.cluster:
			section.check()

		if self.fetch.issue > self.icache.line * 4:
			error(0, 'instruction cache line size must be at least 4 times '
				'larger than the total issue width')

		if len(self.cluster) > 1:
			for idx, section in enumerate(self.cluster):
				if section.send < 1:
					error(section.line_nr,
						'cluster %d cannot send data to others',
						idx)
				if section.receive < 1:
					error(section.line_nr,
						'cluster %d cannot receive data from others',
						idx)


# --- Interpret argv ---

# Print usage if we don't have enough arguments
if len(sys.argv) < 3:
	print('Usage: %s <config.ini> <outdir>' % sys.argv[0])
	print('')
	print('Generates the following files:')
	print('  <outdir>/config.ini: copy of <config.ini> for future reference')
	print('  <outdir>/vex.mm: VEX compiler machine model file')
	print('  <outdir>/vex.cfg: VEX simulator configuration file')
	print('  <outdir>/cluster.count: contains the number of clusters, necessary for')
	print('    the -width compiler command line switch')
	print('  <outdir>/vex.bin: binary configuration file for the aed tool')
	sys.exit(2)

cfg_fname = sys.argv[1]
outdir = sys.argv[2]


# --- Read the configuration file ---
with open(cfg_fname, 'r') as f:
	cfg_str = f.read()


# --- Interpret the configuration file ---
cfg = ConfigFile()
for i, line in enumerate(cfg_str.split('\n')):
	line_nr = i + 1

	# Ignore empty lines and comments
	line = line.split('#', 1)[0].strip()
	if not line:
		continue

	# Handle sections
	if line[0] == '[' and line[-1] == ']':
		name = line[1:-1].strip()
		cfg.append_section(name, line_nr)
		continue

	# Handle key-value lines
	line = line.split('=')
	if len(line) != 2:
		warn(line_nr,
			'ignoring line due to syntax error')
		continue
	key, value = map(str.strip, line)
	cfg.append_data(key, value, line_nr)


# --- Check the configuration file ---
cfg.check()


# --- Create the output directory ---
if not os.path.exists(outdir):
	os.makedirs(outdir)


# --- Copy the configuration file into the output directory ---
with open(outdir + os.sep + 'config.ini', 'w') as f:
	f.write(cfg_str)


# --- Generate the compiler configuration file ---
with open(outdir + os.sep + 'vex.mm', 'w') as f:
	s = []
	s.append('# Global configuration')
	s.append('RES: IssueWidth     %d' % cfg.fetch.issue)
	s.append('RES: MemLoad        %d' % cfg.dcache.load)
	s.append('RES: MemStore       %d' % cfg.dcache.store)
	s.append('RES: MemPft         0')
	s.append('')
	for idx, ccfg in enumerate(cfg.cluster):
		s.append('# Cluster %d configuration' % idx)
		s.append('RES: IssueWidth.%d   %d' % (idx, ccfg.issue))
		s.append('RES: Alu.%d          %d' % (idx, ccfg.alu))
		s.append('RES: Mpy.%d          %d' % (idx, ccfg.mul))
		s.append('RES: Memory.%d       %d' % (idx, ccfg.mem))
		s.append('RES: CopySrc.%d      %d' % (idx, ccfg.send))
		s.append('RES: CopyDest.%d     %d' % (idx, ccfg.receive))
		s.append('REG: $r%d            %d' % (idx, ccfg.gp))
		s.append('REG: $b%d            %d' % (idx, ccfg.br))
		s.append('DEL: AluR.%d         0' % idx)
		s.append('DEL: Alu.%d          0' % idx)
		s.append('DEL: CmpBr.%d        0' % idx)
		s.append('DEL: CmpGr.%d        0' % idx)
		s.append('DEL: Select.%d       0' % idx)
		s.append('DEL: Multiply.%d     1' % idx)
		s.append('DEL: Load.%d         1' % idx)
		s.append('DEL: LoadLr.%d       1' % idx)
		s.append('DEL: Store.%d        0' % idx)
		s.append('DEL: Pft.%d          0' % idx)
		s.append('DEL: CpGrBr.%d       0' % idx)
		s.append('DEL: CpBrGr.%d       0' % idx)
		s.append('DEL: CpGrLr.%d       0' % idx)
		s.append('DEL: CpLrGr.%d       0' % idx)
		s.append('DEL: Spill.%d        0' % idx)
		s.append('DEL: Restore.%d      1' % idx)
		s.append('DEL: RestoreLr.%d    1' % idx)
		s.append('')
	s.append('# Verbosity configuration')
	s.append('CFG: Quit           0')
	s.append('CFG: Warn           0')
	s.append('CFG: Debug          0')
	s.append('')
	s.append('')
	f.write('\n'.join(s))


# --- Generate the simulator configuration file ---
with open(outdir + os.sep + 'vex.cfg', 'w') as f:
	s = []
	s.append('# Instruction cache')
	s.append('lg2ICacheSize        %d' % log2(cfg.icache.size))
	s.append('lg2ICacheSets        %d' % log2(cfg.icache.sets))
	s.append('lg2ICacheLineSize    %d' % (3 + log2(cfg.icache.line)))
	s.append('ICachePenalty        %d' % (21 + cfg.icache.line))
	s.append('')
	s.append('# Data cache')
	s.append('lg2CacheSize         %d' % log2(cfg.dcache.size))
	s.append('lg2Sets              %d' % log2(cfg.dcache.sets))
	s.append('lg2LineSize          %d' % (3 + log2(cfg.dcache.line)))
	s.append('MissPenalty          %d' % (21 + cfg.dcache.line))
	s.append('WBPenalty            %d' % (16 + cfg.dcache.line))
	s.append('')
	s.append('# Fixed stuff')
	s.append('CoreCkFreq           100')
	s.append('BusCkFreq            100')
	s.append('NumCaches            1')
	s.append('PrefetchEnable       FALSE')
	s.append('LockEnable           FALSE')
	s.append('StreamEnable         FALSE')
	s.append('lg2StrSize           9')
	s.append('lg2StrSets           4')
	s.append('lg2StrLineSize       5')
	s.append('StrMissPenalty       24')
	s.append('StrWBPenalty         19')
	s.append('ProfGranularity      1.000000')
	s.append('')
	s.append('')
	f.write('\n'.join(s))


# --- Generate the cluster count file ---
with open(outdir + os.sep + 'cluster.count', 'w') as f:
	f.write('%d' % len(cfg.clusters))


# --- Generate the binary config file for aed ---
with open(outdir + os.sep + 'vex.bin', 'wb') as f:

	# Instruction cache information
	f.write(struct.pack('IHH', int(cfg.icache.size*8), int(cfg.icache.line*8), int(cfg.icache.sets)))

	# Data cache information
	f.write(struct.pack('IHH', int(cfg.dcache.size*8), int(cfg.dcache.line*8), int(cfg.dcache.sets)))
	f.write(struct.pack('HH', int(cfg.dcache.load), int(cfg.dcache.store)))

	# Global data
	f.write(struct.pack('HH', int(cfg.fetch.issue), len(cfg.clusters)))

	# Summed cluster data
	f.write(struct.pack('HHHHHHHH',
		int(sum(cfg.clusters.issue)),
		int(sum(cfg.clusters.alu)), int(sum(cfg.clusters.mul)), int(sum(cfg.clusters.mem)),
		int(sum(cfg.clusters.send)), int(sum(cfg.clusters.receive)),
		int(sum(cfg.clusters.gp)), int(sum(cfg.clusters.br))))

	# Individual cluster data
	for cluster in cfg.cluster:
		f.write(struct.pack('HHHHHHHH',
			int(cluster.issue),
			int(cluster.alu), int(cluster.mul), int(cluster.mem),
			int(cluster.send), int(cluster.receive),
			int(cluster.gp), int(cluster.br)))

